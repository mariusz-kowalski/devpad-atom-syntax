class Klass < Parent
  def initialize(param)
    @instance_var = param
  end

  def method
    lmb = ->(param) { puts param }
    l2 = -> { puts "no params" }
    puts "this is method".upcase
    @instance_var.reject { |var| var += 'g'; var == 'wrong' }
    if str =~ /\Aabc[0-9]{1,3}(red|yellow|green).*[^zxc]?/m
      # print some nice information
      puts "String #{str if 2 >= 1} match!";
      @@class_var = false ? "+" : "-"
    end
  end
end

object = Klass.new

arr = [1, 2, 3]
arr do |element|
  print element * 2
end
arr << 4

hash = { key1: "val1", key2: "val2" }
