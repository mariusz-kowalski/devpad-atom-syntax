function rnd(x){
  return Math.round(Math.random() * x);
}

function svg(n){
  var str = "@svg" + n + ": \"";
  for(i=0; i<100; i++){
    var width = rnd(60) + 10;
    var height = rnd(60) + 10;
    var x = rnd(1000 - width);
    var y = rnd(120 - height);
    // str += "<circle cx='"+x+"' cy='"+y+"' r='"+r+"' />";
    str += "<rect id='rect" + i + "' x='" + x + "' y='" + y + "' width='" + width + "' height='" + height + "' />";
  }
  str += "\";"
  return str;
}

var str = "";
// for(s=0; s<5; s++){
  str += svg(0) + "\n";
// }

console.log(str);
